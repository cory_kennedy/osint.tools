#!/bin/sh
echo "Grabbing IOC's from JSON files and extracting them"
cat *.json | jq -r '.indicators[] | select( .type | contains("domain")) | .value' |uniq > domains.txt
cat *.json | jq -r '.indicators[] | select( .type | contains("ip")) | .value' |uniq > ips.txt
#cat *.json | jq -r '.indicators[] | select( .type | contains("url")) | .value' |uniq > url.txt

echo "Enriching IoC's this may take a minute"
python3 pdns.passivetotal.file.ip.py ips.txt > enriched.ips.txt
#rm -f ips.txt
python3 pdns.passivetotal.file.ip.py domains.txt > enriched.domains.txt
#rm -f domains.txt
echo "Enterprise IoC's created..."

echo "Parsing Enriched Enterprise IoC's and preparing json file"
cat enriched.ips.txt | cut -d " " -f2 | cut -d "+" -f1 | grep -v "Domain" | grep -v "Address:" | sed -ne/./p | uniq > enriched.make.some.iocs.txt
cat enriched.domains.txt | cut -d " " -f2 | cut -d "+" -f1 | grep -v "Domain" | grep -v "Address:" | sed -ne/./p | uniq >> enriched.make.some.iocs.txt

python3 indicator.maker.py enriched.make.some.iocs.txt true > ready.for.merge.json
echo "Enrichment complete. Please review ready.for.merge.json"





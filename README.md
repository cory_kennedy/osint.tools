```
                                            RISKIQ                                                                   
                                    **THREATRESEARCHLABS##%                                                           
                                **(######################%%%%%%                                                       
                             ((((((#####################%%%%%%%%%#                                                    
                           ((((((((####*               #%%%%%%%%####                                                  
                         (((((((((/*                       %%%%#####(/                                                
                        (((((((/**   ,////((((((((((((###*   %#####((//                                               
                       ((((((***%%%%%%%%%%#(((((((((((#########(/#((////                                              
                      ,((((****#%%%%%%%%%%%%%%%%%###((############//////,                                             
                     %((((****/##%%%%%%%%%(,       /#################/////                                            
                   %%%(((/////(###                           ((########/////                                          
                 %%%%%((//////                                   */###%%(((///                                        
               (%%%%%%(///////, _____________________.____       **/%%%((((((/                                                    
              %%%%%%%%%///////, \__    ___/\______   \    |      ////(%%(((((((                                                   
             %%%%%%%%% ///////,,  |    |    |       _/    |      (((((, %#(((((((                                                  
             %%%%%%%%   ///////,  |    |    |    |   \    |___   ###(,   %(((((((                                                  
             %%%%%%%,    ((((((,  |____|    |____|_  /_______ \  %%*,,   %((((((*                                                  
            %%%%%%%,,     (((((*,                  \/        \/ #%#*,    %((((((,                                      
             ###%%%,,      (((((,,,,                       ####*****.     ((((((,,                                    
             ######,,,      (((((,,,,*                   ####******      ((((((,,,                                    
              #####,,,,      .(((/,,,***               ###********      (((((/,,,                                     
               ####/,,,*.      (((/,,****/*         /(/*********       ((((/,,,,                                      
                (((#,,,****      ((/,****////*   ((***********      ((((/*****,                                       
                  ((((,*****/*,    .(****/////*************     *///*********                                         
                    (((/****//////,,,,,****************(/******************                                           
                       (((**//////////(((((((((((((((*******************                                              
                           ((/////////(((((((((((,,,,,,,************
						                        OSINT ENRICHER
                                                
```                                     

### What is this repository for? ###

* I am just a silly little utility for enriching OSINT articles with Enterprise indicators 

### Workflow ###


```           
                          
                          ┌───────────────┐
                          │     OSINT     │
                          └───────────────┘
                                  │
                                  │Export OSINT Article from https://sf.riskiq.net/crawlview/osintArticle/list 
                                  ▼                       ┌───────────────┐
                          ┌ ─ ─ ─ ─ ─ ─ ─ ┐               │   Extracts    │
                          OSINT.Enricher.sh       ───────▶│ Domain's & IP │   <-----(Review & Merge )                        
                          └ ─ ─ ─ ─ ─ ─ ─ ┘               │  from *.json  │             │
                                  │                       └───────────────┘             │
                                  │                                                     │
                                  ▼ Creats Files containing data to enrich              │
                                  (ips.txt, Domains.txt)                                │
                          ┌ ─ ─ ─ ─ ─ ─ ─ ┐                                             │
                               Query PT                                                 │
                          └ ─ ─ ─ ─ ─ ─ ─ ┘                                             │
                                  │                                                     │
                                  │                                                     │
                                  ▼  Create enriched files                              │
                                     enriched.domains.txt & enriched.ips.txt            │
                                                                │                       │
                          ┌ ─ ─ ─ ─ ─ ─ ─ ┐                                             Λ
                              Prep Output                                              ╱ ╲
                          └ ─ ─ ─ ─ ─ ─ ─ ┘                                           ╱   ╲
                                  │                                                  ╱     ╲
                                  │                                                 ╱       ╲
                                  ▼                                                ╱         ╲
          ┌───────────────────────────────────────────────┐                       ╱           ╲
          │    enriched.make.some.iocs.txt                │                    ready.for.merge.json  
          │                                               │─ ─ ─  ─ ─  ─ ─ ▶    ▕               ▏
          └───────────────────────────────────────────────┘                      ╲             ╱
                                                                                  ╲           ╱
                                                                                   ╲         ╱
                                                                                    ╲       ╱
                                                                                     ╲     ╱
                                                                                      ╲   ╱
                                                                                       ╲ ╱
                                                                                        V
            
                                                                          
```
![LikeThis](https://bitbucket.org/cory_kennedy/osint.tools/raw/68eb64f0e9ad36c28821f1446acff88c116e79ad/osint.gif)


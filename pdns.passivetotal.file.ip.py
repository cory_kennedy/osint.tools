import requests,re
from prettytable import PrettyTable
import ujson as json

def get_pdns_pt(domain):
	username='cory.kennedy@riskiq.net'
	key = '2f27825081c9b9bac0e537ac47360d18e213263a7f6c07985be89670614d3b8e'
	headers = {'Content-Type': 'application/json'}
	data = {"query": domain}
	url = 'https://api.passivetotal.org/v2/dns/passive'
	try:
		r = requests.get(url,headers=headers,auth=(username, key),data=json.dumps(data))
		response = json.loads(r.text)
		if response['results']:
			return response['results']
	except:
		print ("Something went wrong with the Domain: %s" % domain)
		pass

def pt_whois_domain(domain):
	username='cory.kennedy@riskiq.net'
	key = '2f27825081c9b9bac0e537ac47360d18e213263a7f6c07985be89670614d3b8e'
	headers = {'Content-Type': 'application/json'}
	data = {"query": domain}
	url = 'https://api.passivetotal.org/v2/whois'
	try:
		r = requests.get(url,headers=headers,auth=(username, key),data=json.dumps(data))
		response = r.json()
		if response:
			return response
		else:
			print("No results for: "+domain)
	except:
		print ("Something went wrong with the Domain: %s" % domain)
		pass

def pt_whois_search(typeof,query):
	#Allowed values: "email", "domain", "name", "organization", "address", "phone", "nameserver"
	username='jon.gross@riskiq.net'
	key = '2de27fcc8040c95a0478a77f27dcd2fc7fa65893316025400723b8a988a5c83e'
	headers = {'Content-Type': 'application/json'}
	data = {"query": query,"field": typeof}
	url = 'https://api.passivetotal.org/v2/whois/search'
	try:
		r = requests.get(url,headers=headers,auth=(username, key),data=json.dumps(data))
		response = r.json()
		if 'results' in response:
			return response['results']
		else:
			print("No results for: "+domain)
	except:
		print ("Something went wrong with the Domain: %s" % domain)
		pass

def print_pt_whois_domain(response):
	details = ["admin","billing","registrant","tech"]
	hasdata = list()
	for key in details:
		if response[key]:
			data = response[key]
			hasdata.append([key,data])
	print("Domain: "+response['domain'])
	print("Registrar: "+response['registrar'])
	print("Registration Date: "+response['registered'])
	print("Expiration Date: "+response['expiresAt'])
	print(hasdata[0][0].title()+" Contact Details:")
	for key in hasdata[0][1]:
		print('  '+key.title()+': '+hasdata[0][1][key])
	for i in range(0,len(hasdata)-1):
		if hasdata[0][1] == hasdata[i+1][1]:
			continue
		else:
			for key in hasdata[i+1][1]:
				print("Data for "+hasdata[i+1][0].title()+" contact is different:")
				print('\t'+key.title()+': '+hasdata[0][1][key])

def parse_pdns_pt_domain(results):
	domains = list()
	result_pairs = {}
	ips = list()
	for result in results:
		domain = result['value'].rstrip('.')
		rectype = result['recordType']
		restype = result['resolveType']
		first = result['firstSeen']
		last = result['lastSeen']
		domains.append(domain)
		if rectype == 'A' and restype == 'ip':
			try:
				if result_pairs[domain]:
					result_pairs[domain].append({result['resolve']:{"FirstSeen":first,"LastSeen":last}})
					ips.append(result['resolve'])
			except:
				result_pairs[domain] = [{result['resolve']:{"FirstSeen":first,"LastSeen":last}}]
				ips.append(result['resolve'])
	unique_domains = list(set(domains))
	unique_domains.sort()
	unique_ips = list(set(ips))
	unique_ips.sort()
	return (unique_domains,unique_ips,result_pairs)

def parse_pdns_pt_ip(results):
	result_pairs = {}
	dns_resolutions = list()
	ip_address = results[0]['value']
	for result in results:
		if result['recordType'] == 'A':
			dns_resolutions.append(result['resolve'])
			try:
				result_pairs[result['resolve']] = {"FirstSeen":result['firstSeen'],"LastSeen":result['lastSeen'],"Sources":result['source']}
			except:
				print("Try Again?")
	unique_domains = list(set(dns_resolutions))
	return (ip_address,unique_domains,result_pairs)

def display_pdns_pt_ip(parsed):
	print ("\nIP Address: "+parsed[0])
	uniq_d = parsed[1]
	res = parsed[2]
	print("\nUnique Domains:")
	for domain in uniq_d:
		print(domain)
	print("\nResolutions:")
	for domain in res:
		print("\t" + domain + '\tFirstSeen: ' + res[domain]['FirstSeen'] + '\tLastSeen: '+res[domain]['LastSeen'])

def display_pdns_table_ip(parsed):
	uniq_d = parsed[1]
	res = parsed[2]
	if len(uniq_d) > 1000:
		print("IP: "+parsed[0]+" has too Many Unique Resolutions...Skipping")
	else:
		print ("\nIP Address: "+parsed[0])
		all_res = list()
		dom = PrettyTable()
		dom.field_names = ["Domain","FirstSeen","LastSeen"]
		dom.align["Domain"] = "l"
		dom.align["FirstSeen"] = "r"
		dom.align["LastSeen"] = "r"
		for domain in res:
			all_res.append((domain,res[domain]['FirstSeen'],res[domain]['LastSeen']))
		sorted_by_firstseen = sorted(all_res, key=lambda l: l[1],reverse=True)
		for s in sorted_by_firstseen:
			dom.add_row(s)
		print(dom)
		print("\n")

def display_pdns_table_domain(parsed):
	uniq_d = parsed[0]
	uniq_ip = parsed[1]
	res = parsed[2]
	d = PrettyTable()
	d.field_names = ["Unique Domains"]
	d.align["Unique Domains"] = "l"
	for domain in uniq_d:
		d.add_row([domain])
	print(d)
	print("\n")
	i = PrettyTable()
	i.field_names = ["Unique IP Addresses"]
	i.align["Unique IP Addresses"] = "l"
	for ip in uniq_ip:
		i.add_row([ip])
	print(i)
	print("\n")
	for domain in res:
		all_res = list()
		dom = PrettyTable()
		dom.field_names = ["Domain","IP","FirstSeen","LastSeen"]
		dom.align["IP"] = "l"
		dom.align["FirstSeen"] = "r"
		dom.align["LastSeen"] = "r"
		all_res = list()
		for resolved in res[domain]:
			for key in resolved.keys():
				all_res.append((domain,key,resolved[key]['FirstSeen'],resolved[key]['LastSeen']))
		sorted_by_firstseen = sorted(all_res, key=lambda l: l[2],reverse=True)
		for s in sorted_by_firstseen:
			dom.add_row(s)
		print(dom)
		print("\n")

def display_pdns_pt_domain(parsed):
	uniq_d = parsed[0]
	uniq_ip = parsed[1]
	res = parsed[2]
	print("Unique Domains:")
	for domain in uniq_d:
		print(domain)
	print("\nUnique IP Addresses:")
	for ip in uniq_ip:
		print(ip)
	print("\nUnique IP per Domain:")
	for domain in res:
		print('\nDomain Name: '+domain)
		for resolved in res[domain]:
			for key in resolved.keys():
				print("\t" + key + '\tFirstSeen: ' + resolved[key]['FirstSeen'] + '\tLastSeen: '+resolved[key]['LastSeen'])

def get_pdns_riq(domain):
	headers = {'Content-Type': 'application/json'}
	url = 'http://dnsdb-ec2.vip.usw1.riskiq/lookup/rrset/name/*.'+domain+'./A?limit=100000000'
	try:
		r = requests.get(url,headers=headers)
		return r.text
	except:
		print ("Something went wrong with the Domain: %s" % domain)
		pass

def parse_pdns_riq(response):
	resolved = re.findall('.*IN A .*',response)
	ip_resolution = {}
	ips = list()
	domains = list()
	for resolutions in resolved:
		resolution =resolutions.replace('. IN A ',':').split(':')
		domains.append(resolution[0])
		ips.append(resolution[1])
		try:
			if ip_resolution[resolution[0]]:
				ip_resolution[resolution[0]].append(resolution[1])
		except:
				ip_resolution[resolution[0]] = [resolution[1]]
	unique_domains = list(set(domains))
	unique_domains.sort()
	unique_ips = list(set(ips))
	unique_ips.sort()
	return (unique_domains,unique_ips,ip_resolution)


#queries = (row.replace('\n','') for row in open('biodata.5.31'))
#with open('5.31.resolutions','w') as res:
	#for query in queries:
	#	response = get_pdns_pt(query)
	#	if response:
	#		results = parse_pdns_pt_ip(response)
	#		display_pdns_table_ip(results)
	#		uniq_d = results[1]
	#		res.write("IP Address: "+results[0]+'\n')
	#		for domain in uniq_d:
	#			res.write(domain+'\n')


#out = open('otx.subdomains.txt','wb')

#for query in queries:
#	res = get_subs_pt(query)
#	if res:
#		subdomains = parse_subs(res)
#		for subdomain in subdomains:
#			out.write(subdomain.encode('utf-8')+'\n'.encode('utf-8'))
#out.close()

domains = (row.replace('\n','') for row in open('ips.txt'))
for domain in domains:
	response = get_pdns_pt(domain)
	if response:
		results = parse_pdns_pt_ip(response)
		display_pdns_table_ip(results)